/// @description Initialization

global.current_level = 1;	// Set the level number for difficulty tracking in level generator

lives = 4;			// Set starting lives (0 counts as the last life)

randomize();		// Randomize the seed.

inGame = false;
numActiveBalls = 1;

scr_generateLevel();


// ======================
// LIFE-DRAWING VARIABLES
// ======================
row_one = room_height - 32;
row_two = room_height - 16;
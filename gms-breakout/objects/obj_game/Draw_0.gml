/// @description Draw score and lives

draw_set_font(fnt_game_small);
draw_set_halign(fa_right);
draw_text((room_width - 32), (room_height - 32), "SCORE: " + string(score));

lives_position = 0;

// TODO: Change this code to display two lines of lives instead of a single line
for (i = 0; i < lives; i++)
{
	if ((i % 2) == 0)
	{
		lives_position += 32;
		draw_sprite(spr_extra_life, 0, (lives_position + 32), row_one);
	}
	else if ((i % 2) != 0)
	{
		draw_sprite(spr_extra_life, 0, (lives_position + 32), row_two);
	}
}
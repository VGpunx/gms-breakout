/// @description Update method

// Track the number of active blocks
num_blocks = instance_number(obj_block);

if (numActiveBalls <= 0 and lives <= 0)
{
	inGame = false;
	room_goto(room_last);
}
else if (numActiveBalls <= 0 and lives > 0)
{
	lives -= 1;
	numActiveBalls += 1;
	instance_create_layer(obj_paddle.x, (obj_paddle.y - (sprite_height / 2)), "Layer_Ball", obj_ball);
}
else if (num_blocks <= 0 and (room != room_last) and (room != room_first))
{
	// BUG: Before the level resets, if the ball is in a certain location "n"
	// Any block that appears on the next map at that location "n" will
	// return a collision with the ball.
	inGame = false;													// Set inGame flag to false
	obj_background.image_index = irandom(obj_background.num_bkgs);	// Randomize the background
	global.current_level++;											// Increment the level
	scr_generateLevel();											// Generate a new level
}
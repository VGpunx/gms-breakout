/// @description Mouse movement

if (mouse_x >= bounds_right)
{
	x = bounds_right;
}

if (mouse_x <= bounds_left)
{
	x = bounds_left;
}

else if (x != mouse_x and mouse_x < bounds_right and mouse_x > bounds_left)
{
	x = mouse_x;
}
/// @description Initialization

// Set color
enum color
{
	white,
	yellow,
	pink,
	blue,
	red,
	green,
	cyan,
	orange,
	steel,
	gold
};
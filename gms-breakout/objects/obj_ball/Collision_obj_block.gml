/// @description 

// Bounce the ball using a normal bounce
move_bounce_all(true);

// Subtract 1 from obj_block.block_hp
other.block_hp--;
score += 50;

// If no more hp, then kill block
if (other.block_hp <= 0 and obj_game.inGame)
{
	instance_destroy(other);
}
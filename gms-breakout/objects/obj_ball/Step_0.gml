/// @description Update

bounce_frames -= 1;

if(y > room_height)
{
	instance_destroy();
	obj_game.numActiveBalls -= 1;
	obj_game.inGame = false;
}

if (!obj_game.inGame)
{
	x = obj_paddle.x;
	y = obj_paddle.y - (sprite_height / 2);
}
else if (obj_game.inGame)
{
	// Move the object based on dir and spd
	motion_set(direction, speed);
	// Bounce and stuff.
	move_bounce_solid(false);
}

// Horizontal paddle collision check
if (place_meeting(x + hspeed, y, obj_paddle))
{
	if (bounce_frames <= 0)
	{
		hspeed = -hspeed;
		bounce_frames = 60;
	}
}

// Vertical paddle collision check
if (place_meeting(x, y + vspeed, obj_paddle))
{
	if (bounce_frames <= 0)
	{
		ball_coll_x = x							// Get the x coord of the ball
		paddle_coll_x = obj_paddle.x			// Get the x coord of the paddle
		coll_diff = paddle_coll_x - ball_coll_x	// Difference between x of ball and paddle

		paddle_bounce_arc = 90

		angle_offset = coll_diff * (paddle_bounce_arc / obj_paddle.sprite_width)

		// Angle 90 is up, 0 is right, 180 is left, 270 is down
		vspeed = -vspeed
		direction = paddle_bounce_arc + angle_offset

		bounce_frames = 60
	}
}
/// @description Initialization

// The ball sits on the paddle when it is not in play.
x = obj_paddle.x;
y = obj_paddle.y - (sprite_height / 2);

// Set initial direction and speed
// Is applied when SPACE is pressed
direction = 67;
speed = 0;

bounce_frames = 0;
bounce_frames = clamp(bounce_frames, 0, 60);
/// @description 

sprite_index = spr_boss;

// Flag for hit animation
hit = false;

// In play flag
bossInPlay = false;
bossDead = false;

// Difficulty settings
bossLevel = 1;
bossHP = 1;

// Movement controls
bossSpeed = 2;
enum dir {
	left,
	right
}

initialDirection = irandom_range(dir.left, dir.right);

// Screen boundary settings
bounds_left = 92.5;
bounds_right = room_width - 92.5;

// Explosion time
numExplosions = 5;
timeBetweenExplosions = 30;
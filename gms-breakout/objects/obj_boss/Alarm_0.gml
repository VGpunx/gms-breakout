/// @description Initial movement alarm

// Check the initial direction.
if (initialDirection == dir.right) {
	move_right();
} else if (initialDirection == dir.left) {
	move_left();
}
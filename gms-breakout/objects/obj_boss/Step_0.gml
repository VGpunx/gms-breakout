/// @description 

// If HP reaches 0, then destroy the boss.
if (bossHP <= 0) {
	instance_destroy();
}

// Wait for game to start then begin movement after 2 seconds.
if (obj_game.inGame && !bossInPlay) {
	bossInPlay = true;
	alarm[0] = 120;
} else if (!obj_game.inGame && bossInPlay) {
	bossInPlay = false;
}

// If not bossInPlay, then gradually move toward the center of the screen.
if (!bossInPlay) {
	if (x < room_width / 2) {
		hspeed = bossSpeed / 2;
		if (!hit) {
			sprite_index = spr_boss_right;
		}
	} else if (x > room_width / 2) {
		hspeed = -(bossSpeed / 2);
		if (!hit) {
			sprite_index = spr_boss_left;
		}
	} else if (x == room_width / 2) {
		hspeed = 0;
		if (!hit) {
			sprite_index = spr_boss;
		}
	}
}

// Move left
if (x <= bounds_left)
{
	x = bounds_left;
	move_right();
}

// Move right
if (x >= bounds_right)
{
	x = bounds_right;
	move_left();
}



{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 1,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 187,
  "bbox_top": 3,
  "bbox_bottom": 28,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 192,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a941d542-0f6c-4802-a97c-f00473a8ace9","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a941d542-0f6c-4802-a97c-f00473a8ace9","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"LayerId":{"name":"2363d18d-b964-48b8-89e6-fc1e22e8f2a2","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_paddle_left","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"resourceVersion":"1.0","name":"a941d542-0f6c-4802-a97c-f00473a8ace9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6233058f-113d-4cc7-beec-7d2374a0d2af","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6233058f-113d-4cc7-beec-7d2374a0d2af","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"LayerId":{"name":"2363d18d-b964-48b8-89e6-fc1e22e8f2a2","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_paddle_left","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"resourceVersion":"1.0","name":"6233058f-113d-4cc7-beec-7d2374a0d2af","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_paddle_left","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"09d630d7-4bf1-438d-a187-5f4711698924","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a941d542-0f6c-4802-a97c-f00473a8ace9","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"db21f8f0-4575-4282-8356-94d38d300dc9","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6233058f-113d-4cc7-beec-7d2374a0d2af","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 96,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_paddle_left","path":"sprites/spr_paddle_left/spr_paddle_left.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2363d18d-b964-48b8-89e6-fc1e22e8f2a2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "plyr_sprites",
    "path": "folders/Sprites/plyr_sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_paddle_left",
  "tags": [],
  "resourceType": "GMSprite",
}
{
    "id": "5e31f105-4919-4014-bd99-6ff091861e3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 146,
    "bbox_left": 0,
    "bbox_right": 644,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ca9c3b7-0229-48c6-a69e-fe6abb62968c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e31f105-4919-4014-bd99-6ff091861e3c",
            "compositeImage": {
                "id": "2766301f-0734-4e32-bac7-06e986ed8145",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca9c3b7-0229-48c6-a69e-fe6abb62968c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec947f4f-ab90-4364-8ac8-e29da8e66d7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca9c3b7-0229-48c6-a69e-fe6abb62968c",
                    "LayerId": "f6289acb-9c4e-4c7c-905a-a4f3cc72d745"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 147,
    "layers": [
        {
            "id": "f6289acb-9c4e-4c7c-905a-a4f3cc72d745",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e31f105-4919-4014-bd99-6ff091861e3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 645,
    "xorig": 322,
    "yorig": 0
}
function scr_level1() {
	{
		// Set the level background
	
	
		// Get block width and height
		blockWidth = sprite_get_width(spr_blocks);
		blockHeight = sprite_get_height(spr_blocks);
	
		// Initial y coords of each group - used to make horizontal lines of blocks
		// Final row y coords for each group
		groupA_init_y = 128;
		groupA_last_row = groupA_init_y + (blockHeight * 2)	// (y coord of first row) + (blockHeight * (number of additional rows))
		groupB_init_y = 288;
		groupB_last_row = groupB_init_y + blockHeight
		groupC_init_y = 288 + (blockHeight * 5);
		groupC_last_row = groupC_init_y + (blockHeight * 2)
	
		// Set group colors
		groupA_color = irandom(color.orange);
		groupB_color = irandom_range(color.steel, color.gold);
		groupC_color = irandom(color.orange);
	
		// Set group block_hp
		groupA_blockHP = 1;
	
		if (groupB_color == color.steel)
		{
			groupB_blockHP = 2;
		}
		else if (groupB_color == color.gold)
		{
			groupB_blockHP = 3;
		}
	
		groupC_blockHP = 1;
	
		// =======================
		// - CREATE BLOCK GROUPS -
		// =======================
		// blockGroupA
		blockIndex = 0;		// Block iterator
		for (groupA_yCoord = groupA_init_y; groupA_yCoord <= groupA_last_row; groupA_yCoord += blockHeight)
		{
			for (blockX = 96; blockX <= 736; blockX += blockWidth)
			{
				blockGroupA[blockIndex] = instance_create_layer(blockX, groupA_yCoord, "Layer_Blocks", obj_block);
				blockGroupA[blockIndex].image_index = groupA_color;
				blockGroupA[blockIndex].block_color = groupA_color;
				blockGroupA[blockIndex].block_hp = groupA_blockHP;
				blockIndex++;
			}
		}
	
		// blockGroupB
		blockIndex = 0;
		for (groupB_yCoord = groupB_init_y; groupB_yCoord <= groupB_last_row; groupB_yCoord += blockHeight)
		{
			for (blockX = 96; blockX <= 736; blockX += blockWidth)
			{
				blockGroupB[blockIndex] = instance_create_layer(blockX, groupB_yCoord, "Layer_Blocks", obj_block);
				blockGroupB[blockIndex].image_index = groupB_color;
				blockGroupB[blockIndex].block_color = groupB_color;
				blockGroupB[blockIndex].block_hp = groupB_blockHP;
				blockIndex++;
			}
		}
	
		// blockGroupC
		blockIndex = 0;
		for (groupC_yCoord = groupC_init_y; groupC_yCoord <= groupC_last_row; groupC_yCoord += blockHeight)
		{
			for (blockX = 96; blockX <= 736; blockX += blockWidth)
			{
				blockGroupC[blockIndex] = instance_create_layer(blockX, groupC_yCoord, "Layer_Blocks", obj_block);
				blockGroupC[blockIndex].image_index = groupC_color;
				blockGroupC[blockIndex].block_color = groupC_color;
				blockGroupC[blockIndex].block_hp = groupC_blockHP;
				blockIndex++;
			}
		}
	}


}

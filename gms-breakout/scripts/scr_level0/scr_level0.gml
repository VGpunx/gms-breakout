function scr_level0() {
	{		
		// Get block width and height
		blockWidth = sprite_get_width(spr_blocks);
		blockHeight = sprite_get_height(spr_blocks);
	
		// y coords of each group - used to make horizontal lines of blocks
		groupA_yCoord = 224;
		groupB_yCoord = 352;
		groupC_yCoord = 480;
	
		// Set group colors
		groupA_color = irandom(color.orange);
		groupB_color = irandom_range(color.steel, color.gold);
		groupC_color = irandom(color.orange);
	
		// Set group block_hp
		groupA_blockHP = 1;
	
		if (groupB_color == color.steel)
		{
			groupB_blockHP = 2;
		}
		else if (groupB_color == color.gold)
		{
			groupB_blockHP = 3;
		}
	
		groupC_blockHP = 1;
	
		// Set level difficulty
		if (global.current_level < 2)
		{
			level_difficulty = 0;
		}
		else if (global.current_level >= 2)
		{
			level_difficulty = 1;
		}
	
		// =======================
		// - CREATE BLOCK GROUPS -
		// =======================
		// blockGroupA
		blockIndex = 0;		// Block iterator
		for (blockX = 320; blockX <= 512; blockX += blockWidth)
		{
			blockGroupA[blockIndex] = instance_create_layer(blockX, groupA_yCoord, "Layer_Blocks", obj_block);
			blockGroupA[blockIndex].image_index = groupA_color;
			blockGroupA[blockIndex].block_color = groupA_color;
			blockGroupA[blockIndex].block_hp = groupA_blockHP;
			blockIndex++;
		}
	
		// blockGroupA difficulty level-changes
		if (level_difficulty == 1)
		{
			blockGroupA[blockIndex] = instance_create_layer(256, groupA_yCoord, "Layer_Blocks", obj_block);
			blockGroupA[blockIndex].image_index = groupA_color;
			blockGroupA[blockIndex].block_color = groupA_color;
			blockGroupA[blockIndex].block_hp = groupA_blockHP;
			blockIndex++;
		
			blockGroupA[blockIndex] = instance_create_layer(192, groupA_yCoord, "Layer_Blocks", obj_block);
			blockGroupA[blockIndex].image_index = groupA_color;
			blockGroupA[blockIndex].block_color = groupA_color;
			blockGroupA[blockIndex].block_hp = groupA_blockHP;
			blockIndex++;
		
			blockGroupA[blockIndex] = instance_create_layer(576, groupA_yCoord, "Layer_Blocks", obj_block);
			blockGroupA[blockIndex].image_index = groupA_color;
			blockGroupA[blockIndex].block_color = groupA_color;
			blockGroupA[blockIndex].block_hp = groupA_blockHP;
			blockIndex++;
		
			blockGroupA[blockIndex] = instance_create_layer(640, groupA_yCoord, "Layer_Blocks", obj_block);
			blockGroupA[blockIndex].image_index = groupA_color;
			blockGroupA[blockIndex].block_color = groupA_color;
			blockGroupA[blockIndex].block_hp = groupA_blockHP;
			blockIndex++;
		}
	
		// blockGroupB
		blockIndex = 0;
		for (blockX = 256; blockX <= 576; blockX += blockWidth)
		{
			blockGroupB[blockIndex] = instance_create_layer(blockX, groupB_yCoord, "Layer_Blocks", obj_block);
			blockGroupB[blockIndex].image_index = groupB_color;
			blockGroupB[blockIndex].block_color = groupB_color;
			blockGroupB[blockIndex].block_hp = groupB_blockHP;
			blockIndex++;
		}
	
		// blockGroupC
		blockIndex = 0;
		for (blockX = 192; blockX <= 640; blockX += blockWidth)
		{
			blockGroupC[blockIndex] = instance_create_layer(blockX, groupC_yCoord, "Layer_Blocks", obj_block);
			blockGroupC[blockIndex].image_index = groupC_color;
			blockGroupC[blockIndex].block_color = groupC_color;
			blockGroupC[blockIndex].block_hp = groupC_blockHP;
			blockIndex++;
		}
	}


}

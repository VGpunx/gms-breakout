function scr_generateLevel() {
	{
		// Simply turn debug on to jump to the level chosen below
		// This code is for use as I'm continuing to write levels
		// and should be removed prior to release.
		levelGen_debugMode = false;

	
		// Array of level-generation scripts
		numLevels = 3;							// Total number of levels
		chosenLevel = irandom(numLevels - 1);	// Choose a random level to load

		if (levelGen_debugMode == true)	// Code used for debugging
		{	
			// Debug mode jump-to-level
			scr_level0();
			// global.current_level = 5;
		}
		else	// Code used for regular play
		{
			if (global.current_level == 1)		// Always load level 0 first
			{
				scr_level0();	// Level 0 should be constructed to "teach" the game without a tutorial mode
			}
			else if (global.current_level > 1)
			{
				switch(chosenLevel)
				{
					case 0: 
						scr_level0();
						break;
					case 1:
						scr_level1();
						break;
					case 2:
						scr_level2();
						break;
				}
			}
		}	
	}


}

function scr_level2() {
	{
		// Group A
		numBlocksGroupA = 26;
		groupA[0, 0] = 416;
		groupA[0, 1] = 96;
		groupA[1, 0] = 352;
		groupA[1, 1] =128;
		groupA[2, 0] = 480;
		groupA[2, 1] = 128;
		groupA[3, 0] = 288;
		groupA[3, 1] = 160;
		groupA[4, 0] = 544;
		groupA[4, 1] = 160;
		groupA[5, 0] = 224; 
		groupA[5, 1] = 192;
		groupA[6, 0] = 608; 
		groupA[6, 1] = 192;
		groupA[7, 0] = 160;
		groupA[7, 1] = 224;
		groupA[8, 0] = 672;
		groupA[8, 1] = 224;
		groupA[9, 0] = 96;
		groupA[9, 1] = 256;
		groupA[10, 0] = 736;
		groupA[10, 1] = 256;
		groupA[11, 0] = 96;
		groupA[11, 1] = 288;
		groupA[12, 0] = 736;
		groupA[12, 1] = 288; 
		groupA[13, 0] = 96;
		groupA[13, 1] = 320;
		groupA[14, 0] = 736;
		groupA[14, 1] = 320;
		groupA[15, 0] = 96;
		groupA[15, 1] = 352;
		groupA[16, 0] = 736;
		groupA[16, 1] = 352;
		groupA[17, 0] = 160;
		groupA[17, 1] = 384;
		groupA[18, 0] = 672;
		groupA[18, 1] = 384;
		groupA[19, 0] = 224;
		groupA[19, 1] = 416;
		groupA[20, 0] = 608;
		groupA[20, 1] = 416;
		groupA[21, 0] = 288;
		groupA[21, 1] = 448;
		groupA[22, 0] = 544;
		groupA[22, 1] = 448;
		groupA[23, 0] = 352;
		groupA[23, 1] = 480;
		groupA[24, 0] = 480;
		groupA[24, 1] = 480;
		groupA[25, 0] = 416;
		groupA[25, 1] = 512;
	
		// Group B
		numBlocksGroupB = 22;
		groupB[0, 0] = 416;
		groupB[0, 1] = 128;
		groupB[1, 0] = 352;
		groupB[1, 1] = 160;
		groupB[2, 0] = 480;
		groupB[2, 1] = 160;
		groupB[3, 0] = 288;
		groupB[3, 1] = 192;
		groupB[4, 0] = 544;
		groupB[4, 1] = 192;
		groupB[5, 0] = 224;
		groupB[5, 1] = 224;
		groupB[6, 0] = 608;
		groupB[6, 1] = 224;
		groupB[7, 0] = 160;
		groupB[7, 1] = 256;
		groupB[8, 0] = 672;
		groupB[8, 1] = 256;
		groupB[9, 0] = 160;
		groupB[9, 1] = 288;
		groupB[10, 0] = 672;
		groupB[10, 1] = 288;
		groupB[11, 0] = 160;
		groupB[11, 1] = 320;
		groupB[12, 0] = 672;
		groupB[12, 1] = 320;
		groupB[13, 0] = 160;
		groupB[13, 1] = 352;
		groupB[14, 0] = 672;
		groupB[14, 1] = 352;
		groupB[15, 0] = 224;
		groupB[15, 1] = 384;
		groupB[16, 0] = 608;
		groupB[16, 1] = 384;
		groupB[17, 0] = 288;
		groupB[17, 1] = 416;
		groupB[18, 0] = 544;
		groupB[18, 1] = 416;
		groupB[19, 0] = 352;
		groupB[19, 1] = 448;
		groupB[20, 0] = 480;
		groupB[20, 1] = 448;
		groupB[21, 0] = 416;
		groupB[21, 1] = 480;
	
		numBlocksGroupC = 8
		groupC[0, 0] = 352;
		groupC[0, 1] = 288;
		groupC[1, 0] = 352;
		groupC[1, 1] = 320;
		groupC[2, 0] = 416;
		groupC[2, 1] = 256;
		groupC[3, 0] = 416;
		groupC[3, 1] = 288;
		groupC[4, 0] = 416;
		groupC[4, 1] = 320;
		groupC[5, 0] = 416;
		groupC[5, 1] = 352;
		groupC[6, 0] = 480;
		groupC[6, 1] = 288;
		groupC[7, 0] = 480;
		groupC[7, 1] = 320;
	
		// Get block width and height
		blockWidth = sprite_get_width(spr_blocks);
		blockHeight = sprite_get_height(spr_blocks);
	
		// Initial y coords of each group - used to make horizontal lines of blocks
		// Final row y coords for each group
		groupA_init_y = 128;
		groupA_last_row = groupA_init_y + (blockHeight * 2)	// (y coord of first row) + (blockHeight * (number of additional rows))
		groupB_init_y = 288;
		groupB_last_row = groupB_init_y + blockHeight
		groupC_init_y = 288 + (blockHeight * 5);
		groupC_last_row = groupC_init_y + (blockHeight * 2)
	
		// Set group colors
		groupA_color = irandom_range(color.steel, color.gold);
		groupB_color = irandom(color.orange);
		groupC_color = irandom(color.orange);
	
		// Set group block_hp
		if(groupA_color == color.steel)
		{
			groupA_blockHP = 2;
		}
		else if (groupA_color == color.gold)
		{
			groupA_blockHP = 3;
		}
	
		groupB_blockHP = 1;
	
		groupC_blockHP = 1;
	
		// =======================
		// - CREATE BLOCK GROUPS -
		// =======================
		// blockGroupA
		for (i = 0; i < numBlocksGroupA; i++)
		{
				instA[i] = instance_create_layer(groupA[i, 0], groupA[i, 1], "Layer_Blocks", obj_block);
				instA[i].image_index = groupA_color;
				instA[i].block_color = groupA_color;
				instA[i].block_hp = groupA_blockHP;
		}
	
		// blockGroupB
		for (i = 0; i < numBlocksGroupB; i++)
		{
			instB[i] = instance_create_layer(groupB[i, 0], groupB[i, 1], "Layer_Blocks", obj_block);
			instB[i].image_index = groupB_color;
			instB[i].block_color = groupB_color;
			instB[i].block_hp = groupB_blockHP;
		}
	
		// blockGroupC
		for (i = 0; i < numBlocksGroupC; i++)
		{
			instC[i] = instance_create_layer(groupC[i, 0], groupC[i, 1], "Layer_Blocks", obj_block);
			instC[i].image_index = groupC_color;
			instC[i].block_color = groupC_color;
			instC[i].block_hp = groupC_blockHP;
		}
	}


}

# GMS-Breakout

GMS-Breakout is a prototype of the game Arkanoid in GameMaker Studio 2!

This project is one of many prototypes I designed and engineered in GameMaker Studio 2 for use as an example project for my students learning to code. As such, the game is meant to feature very simple controls and an intuitive design intended to demonstrate core GameMaker functionality with a focus on the use of "Scripts" which contain the game's level generation system and other such function calls. The game also features an escalating challenge system that changes the individual level generation scripts to show how a given level can be slightly modified to increase the challenge, given the player's current level.

I had also planned a "boss" paddle which would use a similar bounce function as the one built for the paddle and ball in this game (based on my Py-Pong project). The boss is currently in the game, but remains unfinished. As this was a prototype for use in education, I may come back for subsequent updates in the future.

I had also planned a "shooting" feature which would eventually be the only way to destroy the boss (who would also fire back at the player), leading to much more frantic gameplay.

## Features

- Mouse controls
- Paddle/Ball bounce system
- Simple computer AI
- Level generation system for infinite gameplay
- Escalating challenge

## Installation

I have not currently made any attempt to create an executable for the game and may not make one in the foreseeable future due to recent changes to the way GameMaker licenses work. If I ever actually get around to a release, it will likely be posted to itch.io and I will update this text at that time.

## Requirements

- GameMaker Studio 2 Runtime version 2.3.5.458

## Controls and play instructions

Press Enter to begin
Use the mouse to move the paddle on the screen
Use the mouse button to fire the ball and try to bounce the ball off your paddle without letting it fall off the bottom of the screen!
Use the ball to clear out all the blocks and progress to the next level!
The game will generate levels infinitely from a list, eventually ramping up the challenge as you progress!
